# README #

This module uses Neural Turing Machines for image classification tasks. 
It does so by combining convolutional layers with Lasagne's NTM layers.

Apart from implementing a ConvNTM layer, a CustomController class has been added in order to introduce flexibility
to the Controller architecture.

Contributions may be viewed in the ntm folder in ConvNTMLayer.py and controllers.py

### NTM Lasagne Library

This is the NTM library used by our module: https://github.com/snipsco/ntm-lasagne 

### How do I get set up? ###

The `get\_module` function returns a ConvNTM layer which takes as input: 
	- the images
	- size of the output softmax 

### Performance on MNIST ###

We trained our model on MNIST. The architecture was:
* the first Conv-Pool layer borrowed from Lenet5
* followedy ba (10, 100)-sized memory NTM layer

This 97% testing error on MNIST in just the first iteration and it steadily improves to 99% within 5 iterations. 