import numpy as np

import theano
import theano.tensor as T
import numpy as np
import matplotlib.pyplot as plt

import lasagne
from lasagne.layers import InputLayer, DenseLayer, ReshapeLayer, Conv2DLayer
import lasagne.layers
import lasagne.nonlinearities
import lasagne.updates
import lasagne.objectives
import lasagne.init

from ntm.layers import NTMLayer
from ntm.memory import Memory
from ntm.controllers import DenseController
from ntm.heads import WriteHead, ReadHead
from ntm.updates import graves_rmsprop


class ConvNTMLayer(Layer):
	def __init__(self, incoming,
				batch_size,
				memory_shape,
				controller_type,
				num_units,
				heads,
				only_return_final=False,
				**kwargs):
        super(ConvNTMLayer, self).__init__(incoming, **kwargs)
        self.only_return_final = only_return_final

        self.cnn = lasagne.layers.Conv2DLayer(
	            l_input, num_filters=32, filter_size=(5, 5),
	            nonlinearity=lasagne.nonlinearities.rectify,
	            W=lasagne.init.GlorotUniform())
	    self.pool = lasagne.layers.MaxPool2DLayer(self.cnn, pool_size=(2, 2))
	    output_shape = lasagne.layers.get_output_shape(self.pool)
	    self.pool_reshape = ReshapeLayer(self.pool, (batch_size, 1, np.prod(output_shape[1:])))
        self.memory = Memory(memory_shape, name='memory', memory_init=lasagne.init.Constant(1e-6), learn_init=False)
        if controller_type == 'DenseController':
	        self.controller = DenseController(self.pool_reshape, memory_shape=memory_shape,
		        num_units=num_units, num_reads=1,
		        nonlinearity=lasagne.nonlinearities.rectify,
		        name='controller')
        self.heads = [
		        WriteHead(controller, num_shifts=3, memory_shape=memory_shape, name='write', learn_init=False,
		            nonlinearity_key=lasagne.nonlinearities.rectify,
		            nonlinearity_add=lasagne.nonlinearities.rectify),
		        ReadHead(controller, num_shifts=3, memory_shape=memory_shape, name='read', learn_init=False,
		            nonlinearity_key=lasagne.nonlinearities.rectify)
		    ]
        l_ntm = NTMLayer(self.pool_reshape, memory=memory, controller=controller, heads=heads)

	def get_model(input_var, batch_size=1, size=27, num_units=100, memory_shape=(128, 20)):
	    # Input Layer
	    l_input = InputLayer((batch_size, 1, 28, 28), input_var=input_var)
	    # _, seqlen, _ = l_input.input_var.shape

	    l_cnn = lasagne.layers.Conv2DLayer(
	            l_input, num_filters=32, filter_size=(5, 5),
	            nonlinearity=lasagne.nonlinearities.rectify,
	            W=lasagne.init.GlorotUniform())
	    l_pool = lasagne.layers.MaxPool2DLayer(l_cnn, pool_size=(2, 2))

	    # reshape to vector
	    output_shape = lasagne.layers.get_output_shape(l_pool)
	    l_pool_reshape = ReshapeLayer(l_pool, (batch_size, 1, np.prod(output_shape[1:])))

	    print batch_size, np.prod(output_shape[1:])
	    # Neural Turing Machine Layer
	    memory = Memory(memory_shape, name='memory', memory_init=lasagne.init.Constant(1e-6), learn_init=False)
	    controller = DenseController(l_pool_reshape, memory_shape=memory_shape,
	        num_units=num_units, num_reads=1,
	        nonlinearity=lasagne.nonlinearities.rectify,
	        name='controller')
	    heads = [
	        WriteHead(controller, num_shifts=3, memory_shape=memory_shape, name='write', learn_init=False,
	            nonlinearity_key=lasagne.nonlinearities.rectify,
	            nonlinearity_add=lasagne.nonlinearities.rectify),
	        ReadHead(controller, num_shifts=3, memory_shape=memory_shape, name='read', learn_init=False,
	            nonlinearity_key=lasagne.nonlinearities.rectify)
	    ]
	    l_ntm = NTMLayer(l_pool_reshape, memory=memory, controller=controller, heads=heads)

	    # Output Layer
	    l_ntm_reshape = ReshapeLayer(l_ntm, (-1, num_units))
	    l_dense = DenseLayer(l_ntm_reshape, num_units=10, nonlinearity=lasagne.nonlinearities.softmax, \
	        name='dense')
	    l_output = ReshapeLayer(l_dense, (batch_size, 10))
	    return l_output, l_ntm